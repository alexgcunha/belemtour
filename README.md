# Objetivo criar uma API - BelémTour

- criar uma api para o aplicativo de turismo de belém

# Tarefas

- [x] Configurar a estrutura do projeto em MVC
- [x] Configurar nodemon sucrase -D
- [x] Configurar o .editorConfig eslint -D
- [x] Iniciar o projeto criando o server, routes, app.
- [x] conecta o projeto com banco de dados - MongoDB
- [] criar o schema de places
- [] criar criar o controller de places
- [] teste para ter certeza que esta funcionando corretamente controller.
