import Conservacao from "../models/atrativos-naturais/Conservacao";

class ControllerConservacao {
  async store(req, res) {
    const paths = req.files;
    console.log(paths);
    const imagelugar = paths.map(caminhos => caminhos.path);
    const imageinfo = imagelugar[imagelugar.length - 1];
    imagelugar.pop();

    const {
      longitude,
      latitude,
      nameOriginal,
      namePopular,
      endereco,
      logradouro,
      bairro,
      email,
      telefone,
      site,
      hrFuncionamento,
      servicos,
      descricaoAtrativa
    } = req.body;
    const categoria = "Ilha";
    const conservacao = await Conservacao.create({
      imageLugar: imagelugar,
      longitude,
      latitude,
      nameOriginal,
      namePopular,
      endereco,
      logradouro,
      bairro,
      imageRef: imageinfo,
      email,
      telefone,
      site,
      hrFuncionamento,
      servicos,
      descricaoAtrativa
    });
    return res.json(conservacao);
  }

  async index(req, res) {
    const conservacao = await Conservacao.find();

    return res.json(conservacao);
  }
}

export default new ControllerConservacao();
