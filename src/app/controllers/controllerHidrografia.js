import Hidrografia from "../models/atrativos-naturais/Hidrografia";

class ControllerHidrografia {
  async store(req, res) {
    const paths = req.files;
    const imagelugar = paths.map(caminhos => caminhos.path);
    const imageinfo = imagelugar[imagelugar.length - 1];
    imagelugar.pop();

    const {
      longitude,
      latitude,
      nameOriginal,
      namePopular,
      endereco,
      logradouro,
      bairro,
      email,
      telefone,
      site,
      hrFuncionamento,
      servicos,
      descricaoAtrativa
    } = req.body;

    const hidrografia = await Hidrografia.create({
      imageLugar: imagelugar,
      longitude,
      latitude,
      nameOriginal,
      namePopular,
      endereco,
      logradouro,
      bairro,
      imageRef: imageinfo,
      email,
      telefone,
      site,
      hrFuncionamento,
      servicos,
      descricaoAtrativa
    });
    return res.json(hidrografia);
  }

  async index(req, res) {
    const hidrografia = await Hidrografia.find();
    if (!hidrografia) {
      return res.status(401).json({ error: "Erro na Lista" });
    }
    return res.json(hidrografia);
  }
}

export default new ControllerHidrografia();
