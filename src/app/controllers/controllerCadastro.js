import Cadastro from "../models/Cadastro";

class ControllerCadastro {
  async store(req, res) {
    const { email, name, password } = req.body;

    const image = req.file.path;
    const UserExist = await Cadastro.findOne({ email });

    if (UserExist) {
      return res.status(401).json({ Error: "E-mail já existe" });
    }

    const { avatar } = await Cadastro.create({
      name,
      avatar: image,
      email,
      password
    });
    return res.json({ name, avatar, email });
  }
  async update(req, res) {
    return res.json({ ok: true });
  }
}

export default new ControllerCadastro();
