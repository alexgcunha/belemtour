import Food from "../models/equipamentos-servicos-turistico/Food";

class ControllerFood {
  async store(req, res) {
    const paths = req.files;

    const imagelugar = paths.map(caminhos => caminhos.path);

    const {
      categoria,
      tipo,
      nome,
      cadastur,
      endereco,
      logradouro,
      numero,
      cep,
      bairro,
      telefone,
      site,
      hfuncionamento,
      pagamento,
      reserva,
      tipoServico,
      especialidade
    } = req.body;

    const food = await Food.create({
      imageLugar: imagelugar,
      categoria,
      tipo,
      nome,
      cadastur,
      endereco,
      logradouro,
      numero,
      cep,
      bairro,
      telefone,
      site,
      hfuncionamento,
      pagamento,
      reserva,
      tipoServico,
      especialidade
    });

    return res.json(food);
  }

  async index(req, res) {
    const food = await Food.find();
    if (!food) {
      return res.status(401).json({ error: "Error na lista" });
    }
    return res.json(food);
  }
}

export default new ControllerFood();
