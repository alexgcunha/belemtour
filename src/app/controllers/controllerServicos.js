import Servicos from "../models/equipamentos-servicos-turistico/Servicos";

class ControllerServicos {
  async store(req, res) {
    const paths = req.files;
    const imagelugar = paths.map(caminhos => caminhos.path);

    const {
      categoria,
      nome,
      cadastur,
      endereco,
      logradouro,
      numero,
      cep,
      bairro,
      telefone,
      site
    } = req.body;

    const servicos = await Servicos.create({
      image: imagelugar,
      categoria,
      tipo,
      nome,
      cadastur,
      endereco,
      logradouro,
      numero,
      cep,
      bairro,
      telefone,
      site
    });

    return res.json(servicos);
  }

  async index(req, res) {
    const servicos = await Servicos.find();
    if (!servicos) {
      return res.status(401).json({ error: "Error na lista" });
    }
    return res.json(servicos);
  }
}

export default new ControllerServicos();
