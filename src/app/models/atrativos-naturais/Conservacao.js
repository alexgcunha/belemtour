import { Schema, model } from "mongoose";

const schemaConservacao = new Schema({
  imageLugar: [],
  longitude: {
    type: String,
    required: true
  },
  latitude: {
    type: String,
    required: true
  },
  nameOriginal: {
    type: String,
    required: true
  },
  namePopular: {
    type: String,
    required: true
  },
  endereco: {
    type: String,
    required: true
  },
  logradouro: {
    type: String,
    required: true
  },
  bairro: {
    type: String,
    required: true
  },
  imageRef: {
    type: String,
    required: true
  },
  email: {
    type: String
  },
  telefone: {
    type: String,
    required: true
  },
  site: {
    type: String
  },
  hrFuncionamento: {
    type: String,
    required: true
  },
  servicos: {
    type: String,
    required: true
  },
  descricaoAtrativa: {
    type: String,
    required: true
  }
});

export default model("Conservacao", schemaConservacao);
