import { Schema, model } from "mongoose";
import bcrypt from "bcryptjs";
const schemaCadastro = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    avatar: {
      type: String,
      required: true
    },
    email: {
      type: String,
      unique: true,
      required: true
    },

    password_hash: {
      type: String
    }
  },
  {
    timestamps: true,
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
  }
);

schemaCadastro.virtual("password");

schemaCadastro.pre("save", async function() {
  if (this.name) {
    this.password_hash = await bcrypt.hash(this.password, 8);
  }
});

export default model("Cadastro", schemaCadastro);
