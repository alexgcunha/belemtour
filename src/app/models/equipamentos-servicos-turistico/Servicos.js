import { Schema, model } from "mongoose";

const schemaServicos = new Schema({
  image: [],
  categoria: {
    type: String,
    required: true
  },
  nome: {
    type: String,
    required: true
  },
  cadastur: {
    type: String,
    required: true
  },
  endereco: {
    type: String,
    required: true
  },
  logradouro: {
    type: String,
    required: true
  },
  numero: {
    type: String,
    required: true
  },
  bairro: {
    type: String,
    required: true
  },
  cep: {
    type: String,
    required: true
  },
  telefone: {
    type: String,
    required: true
  },
  site: {
    type: String
  }
});

export default model("Servicos", schemaServicos);
