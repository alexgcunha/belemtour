import { Schema, model } from "mongoose";

const schemaFood = new Schema({
  imageLugar: [],
  categoria: {
    type: String,
    required: true
  },
  tipo: {
    type: String,
    required: true
  },
  nome: {
    type: String,
    required: true
  },
  cadastur: {
    type: String,
    required: true
  },
  endereco: {
    type: String,
    required: true
  },
  logradouro: {
    type: String,
    required: true
  },
  numero: {
    type: String,
    required: true
  },
  bairro: {
    type: String,
    required: true
  },
  cep: {
    type: String,
    required: true
  },
  telefone: {
    type: String,
    required: true
  },
  site: {
    type: String
  },
  hfuncionamento: {
    type: String,
    required: true
  },
  pagamento: {
    type: String,
    required: true
  },
  reserva: {
    type: String,
    required: true
  },
  tipoServico: {
    type: String,
    required: true
  },
  especialidade: {
    type: String,
    required: true
  }
});

export default model("Food", schemaFood);
