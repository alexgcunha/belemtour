import { Schema, model } from "mongoose";

const schemaHospedagem = new Schema({
  imageLugar: [],
  longitude: {
    type: String,
    required: true
  },
  latitude: {
    type: String,
    required: true
  },
  categoria: {
    type: String,
    required: true
  },
  nome: {
    type: String,
    required: true
  },
  cadastur: {
    type: String,
    required: true
  },
  endereco: {
    type: String,
    required: true
  },
  logradouro: {
    type: String,
    required: true
  },
  numero: {
    type: String,
    required: true
  },
  bairro: {
    type: String,
    required: true
  },
  cep: {
    type: String,
    required: true
  },
  telefone: {
    type: String,
    required: true
  },
  site: {
    type: String
  },
  hfuncionamento: {
    type: String,
    required: true
  },
  pagamento: {
    type: String,
    required: true
  },
  reserva: {
    type: String,
    required: true
  }
});

export default model("Hospedagem", schemaHospedagem);
