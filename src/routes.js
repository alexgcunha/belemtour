import { Router } from "express";
import multer from "multer";
import configMulter from "./config/multer";
import authMiddleware from "./middlewares/auth";
import ControllerCadastro from "./app/controllers/controllerCadastro";
import ControllerSession from "./app/controllers/controllerSession";
import ControllerHidrografia from "./app/controllers/controllerHidrografia";
import ControllerConservacao from "./app/controllers/controllerConservacao";
import ControllerFood from "./app/controllers/controllerFood";
import ControllerServicos from "./app/controllers/controllerServicos";
const route = new Router();

route.post(
  "/hidrografias",
  multer(configMulter).array("files"),
  ControllerConservacao.store
);

route.post("/foods", multer(configMulter).array("files"), ControllerFood.store);
route.get("/foods", ControllerFood.index);
route.post(
  "/cadastro",
  multer(configMulter).single("file"),
  ControllerCadastro.store
);
route.get("/hidrografias", ControllerConservacao.index);

route.post(
  "/servicos",
  multer(configMulter).array("files"),
  ControllerServicos.store
);
route.get("/servicos", ControllerServicos.index);

route.post("/session", ControllerSession.store);
route.use(authMiddleware);
route.put("/cadastro", ControllerCadastro.update);

export default route;
